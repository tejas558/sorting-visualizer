document.getElementById("len").addEventListener('change', populate);
document.getElementById("spd").addEventListener('change', changeSpeed);

var divs = [];
var heights = [];
var d = 0;
var increment = 500;

populate();

function populate() {
    d = 0;
    while(divs.length > 0) {
        divs.pop();
        heights.pop();
    }
    const elements = document.getElementsByClassName("bar");
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
    let l = document.getElementById("len").value;
    let center = document.querySelector(".center")
    for(let i = 0; i < l; i++){
        divs.push(document.createElement("div"));
        divs[i].className = "bar";
        let h = center.offsetHeight * (getRandomInt(90)+10)/100;
        heights.push(h);
        divs[i].style.height = h.toString() + "px";
        center.insertAdjacentElement("beforeend", divs[i]);
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

changeSpeed();

function changeSpeed(){
    let spd = document.getElementById("spd").value
    if(spd == 1){
        increment = 20;
    }
    if(spd == 2){
        increment = 50;
    }
    if(spd == 3){
        increment = 100;
    }
    if(spd == 4){
        increment = 300;
    }
    if(spd == 5){
        increment = 500;
    }
}

function setColor(div, height, color){
    window.setTimeout(function(){
        div.style.backgroundColor = color;
        div.style.height = height.toString() + "px";
    }, d += increment);
}

function getVal(div){
    return div.offsetHeight;
}