function bubble(){
    for(var i=0;i<divs.length-1;i++){
        for(var j=0;j<divs.length-i-1;j++){
            setColor(divs[j],heights[j],"yellow");

            if(heights[j]>heights[j+1]){
                setColor(divs[j],heights[j], "red");
                setColor(divs[j+1],heights[j+1], "red");

                var temp=heights[j];
                heights[j]=heights[j+1];
                heights[j+1]=temp;

                setColor(divs[j],heights[j], "red");
                setColor(divs[j+1],heights[j+1], "red");
            }
            setColor(divs[j],heights[j], "blue");
        }
        setColor(divs[j],heights[j], "green");
    }
    setColor(divs[0],heights[0], "green");
}

function quicksort() {
    quick_sort(0,divs.length-1);
}

function quick_partition (start, end){
    var i = start + 1;
    var piv = heights[start] ;
    setColor(divs[start],heights[start],"yellow");

        for(var j =start + 1; j <= end ; j++ ){
            if (heights[ j ] < piv){
                setColor(divs[j],heights[j],"yellow");

                setColor(divs[i],heights[i],"red");
                setColor(divs[j],heights[j],"red");

                var temp=heights[i];
                heights[i]=heights[j];
                heights[j]=temp;

                setColor(divs[i],heights[i],"red");
                setColor(divs[j],heights[j],"red");

                setColor(divs[i],heights[i],"blue");
                setColor(divs[j],heights[j],"blue");

                i += 1;
            }
    }
    setColor(divs[start],heights[start],"red");
    setColor(divs[i-1],heights[i-1],"red");
    
    var temp=heights[start];
    heights[start]=heights[i-1];
    heights[i-1]=temp;

    setColor(divs[start],heights[start],"red");
    setColor(divs[i-1],heights[i-1],"red");

    for(var t=start;t<=i;t++){
        setColor(divs[t],heights[t],"green");
    }

    return i-1;
}

function quick_sort (start, end ){
    if( start < end ){
        
        var piv_pos = quick_partition (start, end ) ;     
        quick_sort (start, piv_pos -1);
        quick_sort (piv_pos +1, end) ;
    }
 }